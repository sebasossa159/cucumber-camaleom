package com.testing.StepDefinition;

import org.openqa.selenium.WebDriver;

import com.testing.Page.AutomationPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AutomationStep {
	
	private WebDriver driver;
    private AutomationPage autoPage;
    
	@Given("^I am on a product detail page$")
	public void i_am_on_a_product_detail_page() throws Exception {
	    autoPage=new AutomationPage(driver);
	    driver=autoPage.chormeDriverConnection();
	    driver.manage().window().maximize();
	    autoPage.visit("http://automationpractice.com/index.php");
	    autoPage.clickLinkTshirts();
	}


	@When("^I select the T-shirts$")
	public void i_select_the_T_shirts() throws Exception {
	   autoPage.seleccionarTshirts();
	}

	@Then("^the product is added to my shopping cart$")
	public void the_product_is_added_to_my_shopping_cart() throws Exception {
	   autoPage.addCart();
	}

}
