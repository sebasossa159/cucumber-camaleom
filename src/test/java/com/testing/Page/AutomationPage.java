package com.testing.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.testing.Base.Base;

public class AutomationPage extends Base {

    //--------LocatorClassName-----
    By checkLocator=By.className("icon-ok");
    
    //-------LocatorXpath--------------
    By linktshirtsLocator=By.xpath("//*[@id=\"block_top_menu\"]/ul/li[3]/a");
    By buscadorLocator=By.xpath("//*[@id=\"searchbox\"]/button");
    By shirtsLocator=By.xpath("//img[@alt='Faded Short Sleeve T-shirts']");
    By addCartLocator=By.xpath("//span[contains(.,'Add to cart')]");
    
	
	
	public AutomationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public void clickLinkTshirts() throws InterruptedException {
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(linktshirtsLocator));
		clickEjecutor(linktshirtsLocator);
	}
	
	public void seleccionarTshirts() throws InterruptedException {
		desplazamiento(600);	
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(shirtsLocator));
		clickEjecutor(shirtsLocator);
	}
	
	public void addCart() {
		desplazamiento(400);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(addCartLocator));
		clickEjecutor(addCartLocator);
		System.out.println(tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(checkLocator)));
	}

}
